package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public abstract class QuestionData {
    private final int questionNumber;
    private final int defaultNext;
    private final String prompt;
    private final boolean mandatory;

    public QuestionData(int questionNumber, int defaultNext, String prompt, boolean mandatory){
        this.questionNumber = questionNumber;
        this.defaultNext = defaultNext;
        this.prompt = prompt;
        this.mandatory = mandatory;
    }

    public int getQuestionNumber(){
        return this.questionNumber;
    }

    public String getPrompt(){
        return this.prompt;
    }

    public int getDefaultNext(){ return this.defaultNext; }

    public boolean getMandatory(){ return this.mandatory; }
}

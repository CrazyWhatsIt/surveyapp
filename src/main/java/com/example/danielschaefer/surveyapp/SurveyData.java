package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public class SurveyData {
    int numQuestions;
    private String[] data;
    private static SurveyData surveyData;

    private SurveyData(int numQuestions){
        this.numQuestions = numQuestions;
        this.data = new String[numQuestions];
    }

    public static SurveyData getSurveyData(int numQuestions){
        if (surveyData == null){
            surveyData = new SurveyData(numQuestions);
        }
        return surveyData;
    }

    private int getNumQuestions(){
        return this.numQuestions;
    }

    public static int numQuestions(){
        return surveyData.getNumQuestions();
    }

    public void setAnswer(int n, String answer){
        data[n - 1] = answer;
    }

    public String getAnswer(int n){
        return this.data[n - 1];
    }


    public void clear(){
        surveyData = null;
    }
}

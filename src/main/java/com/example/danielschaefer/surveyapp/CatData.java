package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public class CatData extends QuestionData{
    private final String[] choices;
    private final String[] skipTo;
    public CatData(int questionNumber, int defaultNext, String prompt, boolean mandatory, String choices, String skipTo){
        super(questionNumber, defaultNext, prompt, mandatory);
        this.choices = choices.split(";");
        this.skipTo = skipTo.split(";");
    }
    public int numChoices(){
        return this.choices.length;
    }

    public int numSkipTos() { return this.skipTo.length; }

    public String getChoice(int n){
        return this.choices[n];
    }
    public String getSkipTo(int n){
        return this.skipTo[n];
    }
}

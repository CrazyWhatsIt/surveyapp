package com.example.danielschaefer.surveyapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;
import android.widget.SeekBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    private QuestionDBInterface questionDB = QuestionDB.getQuestionDB(); //create question database
    private AnswerDBInterface answerDB = AnswerDB.getAnswerDB(); //Create responce database
    private int totalQuestions = questionDB.numberOfQuestions(); //get total number of questions
    private SurveyData surveyData = SurveyData.getSurveyData(totalQuestions); //create place to store survey data
    private QuestionData currentQuestion = questionDB.getQuestion(1); // get the first question
    private int questionCount = 1; // current question number
    private Stack<Integer> previousQuestions = new Stack<Integer>(); // stores previous questions in order

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Register back button and next button click listener
        Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(buttonListener);
        Button nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(buttonListener);

        SeekBar seekBar = (SeekBar) findViewById(R.id.answerSeekBar);
        seekBar.setVisibility(View.INVISIBLE);
        // display the first question
        displayQuestion(currentQuestion); // display the first question
    }//onCreate

    //create new onClickListener for back/next button clicks
    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // Get the buttons. This distinguishes the content of the input v.
            Button nextButton = (Button) findViewById(R.id.nextButton);//get next button
            Button backButton = (Button) findViewById(R.id.backButton);//get back button

            /*
               First, find the question type. Then, get the question answer and store in answerDB.
               Finally, Remove the relevant question component.
             */

            String answer = ""; // Storage for answer
            GridLayout gridLayout = (GridLayout) findViewById(R.id.GridView); //get the gridlayout
            if (currentQuestion.getClass().equals(FreeData.class)) { //if the question is freedata
                EditText answerEditText = (EditText) findViewById(R.id.answerEditText); //get the edittext
                answer = answerEditText.getText().toString(); //set the content of the edittext to the answer
                if (!answer.equals("") || !currentQuestion.getMandatory() || v.equals(backButton)) { //if the answer is  not empty, the question isn't mandatory, or the back button has been pressed
                    gridLayout.removeView(answerEditText); // remove the relevant component
                } // otherwise answer == "" and mandatory check is triggered later
            } else if (currentQuestion.getClass().equals(CatData.class)) { // if the question is categorical data
                RadioGroup answerRadioGroup = (RadioGroup) findViewById(R.id.answerRadioGroup); // get the radiogroup
                if (answerRadioGroup.getCheckedRadioButtonId() != -1 || !currentQuestion.getMandatory() || v.equals(backButton)) { // if the question has been answered, the question isn't mandatory, or the back button has been pressed
                    answer = Integer.toString(answerRadioGroup.getCheckedRadioButtonId()); // store the answer
                    gridLayout.removeView(answerRadioGroup); // remove the relevant component
                } // otherwise answer == "" and mandatory check is triggered later
            } else if (currentQuestion.getClass().equals(RangeData.class)) { // if the question is range data
                SeekBar answerSeekBar = (SeekBar) findViewById(R.id.answerSeekBar); // get the seek bar
                RangeData rangeData = (RangeData) currentQuestion; // cast the question as range data
                answer = Integer.toString(answerSeekBar.getProgress() - rangeData.getMin()); // get the answer
                TextView minView = (TextView) findViewById(R.id.minTextView); // get the minimum display
                TextView maxView = (TextView) findViewById(R.id.maxTextView); // get the maximum display
                minView.setVisibility(View.INVISIBLE); // set the minimum display to invisible
                maxView.setVisibility(View.INVISIBLE); // set the maximum display to invisible
                answerSeekBar.setVisibility(View.INVISIBLE); // set the seek bar to invisible
            } else if (currentQuestion.getClass().equals(TextData.class)) { // if the data is text
                answer = "Text Read"; // indicate that this question was reached in the answer data
            } else { // otherwise, throw a new exception
                try {
                    throw new Exception("buttonListener Could Not Identify Data Object");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /*
                Check to see if the survey is done, and if it is, return to the original state.
                Otherwise, Check to see if the question is back or next. Then, get and display the
                new question. Do not allow unanswered questions to advance on mandatory setting.
             */

            // if survey is done, store answers, revert to original state
            if (questionCount == totalQuestions && v.equals(nextButton)) { // test to see if survey is done
                answerDB.appendResponse(); // add response to data file
                surveyData.clear(); // clear old survey answers
                questionDB.clear(); // clear question DB in case of change
                questionDB = QuestionDB.getQuestionDB(); // refresh question DB
                surveyData = SurveyData.getSurveyData(questionDB.numberOfQuestions()); // create new survey data object
                currentQuestion = questionDB.getQuestion(1); // get the first question
                questionCount = 1; // reset current question number
                displayQuestion(currentQuestion); // display the first question
            } else { //if survey is not done
                TextView feedbackText = (TextView) findViewById(R.id.feedBackTextView); // grab the feedback text view
                feedbackText.setVisibility(View.INVISIBLE); // set it to invisible in case mandatory question was just answered
                surveyData.setAnswer(questionCount, answer);// add answer to survey data, including empty strings
                // check to see if back button or next button was pressed
                if (v.equals(backButton)) { // if the back button was pressed
                    questionCount = previousQuestions.pop(); // set the question count to the previous question number
                } else if (v.equals(nextButton)) { // if the next button was pressed
                    if (currentQuestion.getMandatory() && answer.equals("")) { //Check to see if question is mandatory
                        feedbackText.setText(R.string.mandatory_message);
                        feedbackText.setVisibility(View.VISIBLE);
                    } else { // if the question is not mandatory
                        previousQuestions.push(questionCount); // save the last question number
                        if (currentQuestion.getClass().equals(CatData.class)) { //if the class is categorical
                            CatData catData = (CatData) currentQuestion; // cast the question as CatData
                            if (!(catData.numSkipTos() == 1) && !answer.equals("")) { //if there are skip tos indicated
                                String skipTo = catData.getSkipTo(Integer.parseInt(answer));//get the next question number
                                if (!(skipTo.equals(""))) { // if there is a next number indicated
                                    questionCount = Integer.parseInt(skipTo); // set the question count to the next question
                                } else { // if there is no skipTo value
                                    if (catData.getDefaultNext() != -1) { // if there is a default
                                        questionCount = catData.getDefaultNext(); // set question count to default
                                    } else { // if there is not a default
                                        questionCount++; // increment questionCount
                                    }
                                }
                            } else { // if there are no skip tos in question
                                if (catData.getDefaultNext() != -1) { // if there is a default
                                    questionCount = catData.getDefaultNext(); // set the question count to the default
                                } else { // if there is no default
                                    questionCount++; // increment the question count
                                }
                            }
                        } else { // if the question is not categorical
                            if (currentQuestion.getDefaultNext() != -1) { // if there is a default next
                                questionCount = currentQuestion.getDefaultNext(); // set the question count to the default
                            } else { // if there is no default next
                                questionCount++; // increment question count
                            }
                        }
                    }
                }
                if (!currentQuestion.getMandatory() || !answer.equals("")) { // Only update question if mandatory requirements are met
                    currentQuestion = questionDB.getQuestion(questionCount); // get the new question
                    displayQuestion(currentQuestion); // display the new question
                }
            }
        }//end onClick() override
    };//end buttonListener


    // This method displays information associated with a specific question in the UI
    private void displayQuestion(QuestionData question) {

        // set up the question number text view
        TextView questionNumber = (TextView) findViewById(R.id.questionNumberTextView); //get the question number text view
        String qnumber = Integer.toString(questionCount); //set up textview string
        String qtotal = Integer.toString(totalQuestions);
        String qnumberText = ("Question " + qnumber + " of " + qtotal);
        questionNumber.setText(qnumberText); //set question number to text view

        // grab the back and next buttons for editing
        Button nextButton = (Button) findViewById(R.id.nextButton);//get next button
        Button backButton = (Button) findViewById(R.id.backButton);//get back button

        // Change Next button text to Done on last question
        if (questionCount == (totalQuestions)) {
            nextButton.setText("Done");
        } else {
            nextButton.setText("Next");
        }

        //disable back button on first question
        if (questionCount == 1) {
            backButton.setEnabled(false);
        }
        if (questionCount > 1) {
            backButton.setEnabled(true);
        }

        // set up the prompt text view
        String prompt = question.getPrompt(); // get the prompt string
        TextView promptTV = (TextView) findViewById(R.id.promptTextView); // get the prompt text view
        promptTV.setText(prompt); // set the prompt to the text view

        // set up answer box
        GridLayout gridLayout = (GridLayout) findViewById(R.id.GridView);//get the gridlayout

        // Check the question type, then set up the associated information
        if (question.getClass().equals(FreeData.class)) {
            EditText editText = new EditText(this);
            editText.setId(R.id.answerEditText);
            editText.setText(surveyData.getAnswer(questionCount));
            gridLayout.addView(editText, new GridLayout.LayoutParams(
                    GridLayout.spec(2, GridLayout.LEFT),
                    GridLayout.spec(0, GridLayout.LEFT)
            ));
        } else if (question.getClass().equals(CatData.class)) {
            CatData catData = (CatData) question;
            String choice = "";
            String oldAnswer = surveyData.getAnswer(questionCount);
            RadioButton button = new RadioButton(this);
            RadioGroup choicegroup = new RadioGroup(this);
            for (int i = 0; i < catData.numChoices(); i++) {
                button.setId(0 + i);
                if (oldAnswer != null && !oldAnswer.equals("") && Integer.parseInt(oldAnswer) == i) {
                    button.setChecked(true);
                }
                choice = catData.getChoice(i);
                button.setText(choice);
                choicegroup.addView(button, i);
                button = new RadioButton(this);
            }
            choicegroup.setOrientation(RadioGroup.VERTICAL);
            choicegroup.setId(R.id.answerRadioGroup);
            gridLayout.addView(choicegroup, new GridLayout.LayoutParams(
                    GridLayout.spec(2, GridLayout.LEFT),
                    GridLayout.spec(0, GridLayout.LEFT)
            ));
        } else if (question.getClass().equals(RangeData.class)) {
            SeekBar seekBar = (SeekBar) findViewById(R.id.answerSeekBar);
            TextView minView = (TextView) findViewById(R.id.minTextView);
            TextView maxView = (TextView) findViewById(R.id.maxTextView);
            minView.setVisibility(View.VISIBLE);
            maxView.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.VISIBLE);
            RangeData rangeData = (RangeData) question;
            seekBar.setMax(rangeData.getMax() - rangeData.getMin());
            minView.setText(Integer.toString(rangeData.getMin()));
            maxView.setText(Integer.toString(rangeData.getMax()));
            if (surveyData.getAnswer(questionCount) != null) {
                seekBar.setProgress(Integer.parseInt(surveyData.getAnswer(questionCount)));
            }
        } else if (question.getClass().equals(TextData.class)) {
            System.out.println("Displaying Text Question, Do Nothing");
        } else { // otherwise throw exception
            try {
                throw new Exception("displayQuestions Could Not Identify Question Data Object");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }//end displayQuestion()

}//end main activity



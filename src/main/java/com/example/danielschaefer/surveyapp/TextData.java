package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/26/16.
 */
public class TextData extends QuestionData {
    public TextData(int questionNumber, int defaultNext, String prompt, boolean mandatory){
        super(questionNumber, defaultNext, prompt, mandatory);
    }
}

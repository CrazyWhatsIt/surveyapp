package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public abstract class QuestionDBInterface {
    abstract int numberOfQuestions();
    abstract QuestionData getQuestion(int n);
    abstract void clear();
}

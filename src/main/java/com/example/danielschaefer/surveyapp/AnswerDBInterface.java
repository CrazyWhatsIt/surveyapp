package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public abstract class AnswerDBInterface {
    abstract void appendResponse();
}

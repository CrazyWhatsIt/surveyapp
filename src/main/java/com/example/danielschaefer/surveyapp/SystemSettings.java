package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/31/16.
 */
public class SystemSettings {
    public static final String INPUTFILE = "/QuestionFile.csv";
    public static final String OUTPUTFILE = "/AnswerFile.tsv";
    public static final String APPDIR = "/Survey_Data";
    public static final String INPUTDIR = "/QuestionData";
    public static final String OUTPUTDIR = "/ResponseData";


}

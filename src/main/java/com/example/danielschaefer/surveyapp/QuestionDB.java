package com.example.danielschaefer.surveyapp;

import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import android.os.Environment;
/**
 * Created by danielschaefer on 5/5/16.
 */
public class QuestionDB extends QuestionDBInterface{
    private ArrayList<QuestionData> questionData = new ArrayList<>();
    private static QuestionDB questionDB;

    private QuestionDB(){
        BufferedReader br = null;
        try{br = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory() +
                SystemSettings.APPDIR + SystemSettings.INPUTDIR + SystemSettings.INPUTFILE));}
        catch(IOException exception){exception.printStackTrace();}
        try{
            String line = "";
            try {line = br.readLine();}
            catch(NullPointerException exception){exception.printStackTrace();}
            while(line != null){
                String[] splitLine = line.split(",", -1);
                for (int i = 0 ; i < splitLine.length ; i++){
                    System.out.println("ITEM " + i + " IS " + splitLine[i]);
                }
                int questionNumber = Integer.parseInt(splitLine[0]);
                String type = splitLine[1];
                int min = 0;
                if (!splitLine[2].equals("")){min = Integer.parseInt(splitLine[2]);}
                int max = 0;
                if (!splitLine[3].equals("")){max = Integer.parseInt(splitLine[3]);}
                String prompt = splitLine[4];
                String choices = splitLine[5];
                String skipToString = splitLine[6];
                int defaultNext = -1;
                if (!splitLine[7].equals("")){defaultNext = Integer.parseInt(splitLine[7]);}
                boolean mandatory = false;
                if (splitLine[8].equals("T")){
                    mandatory = true;
                }
                QuestionData question = null;
                switch(type) {
                    case "Free": question = new FreeData(questionNumber,defaultNext,prompt,mandatory);
                        break;
                    case "Cat": question = new CatData(questionNumber,defaultNext,prompt,mandatory,choices,skipToString);
                        break;
                    case "Range": question = new RangeData(questionNumber,defaultNext,prompt,mandatory,min,max);
                        break;
                    case "Text": question = new TextData(questionNumber,defaultNext,prompt,mandatory);
                        break;
                    default:
                        try{throw new Exception("Could not recognize question type");}
                        catch(Exception e){e.printStackTrace();}
                }
                questionData.add(question);
                line = br.readLine();
            }
            br.close();
        }catch(IOException exception){exception.printStackTrace();}
    }

    public static QuestionDB getQuestionDB(){
        if (questionDB == null){questionDB = new QuestionDB();}
        return questionDB;
    }

    public int numberOfQuestions(){
      return questionData.size();
    }

    public QuestionData getQuestion(int n){
        return questionData.get(n - 1);
    }

    public void clear(){questionDB = null;}
}

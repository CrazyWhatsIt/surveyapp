package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public class RangeData extends QuestionData{
    private final int min;
    private final int max;
    public RangeData(int questionNumber, int defaultNext, String prompt, boolean mandatory, int min, int max){
        super(questionNumber, defaultNext, prompt, mandatory);
        this.min = min;
        this.max = max;
    }

    public int getMin(){
        return this.min;
    }
    public int getMax(){
        return this.max;
    }
}

package com.example.danielschaefer.surveyapp;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
/**
 * Created by danielschaefer on 5/5/16.
 */
public class AnswerDB extends AnswerDBInterface{

    private static AnswerDB answerDB;

    private AnswerDB(){}

    public static AnswerDB getAnswerDB(){
        if (answerDB == null){answerDB = new AnswerDB();}
        return answerDB;
    }


    public void appendResponse() {
        String line = "";
        for (int i = 1 ; i <= SurveyData.numQuestions() ; i++){
            String addition = "";
            SurveyData surveyData = SurveyData.getSurveyData(SurveyData.numQuestions());
            if (surveyData.getAnswer(i) == null){addition = "";}
            else {addition = surveyData.getAnswer(i);}
            line += addition.replace("  ","");
            line += "   ";
        }
        File file = new File(Environment.getExternalStorageDirectory() + SystemSettings.APPDIR +
            SystemSettings.OUTPUTDIR + SystemSettings.OUTPUTFILE);
        if (!file.exists()){
            try{file.createNewFile();}
            catch(IOException exception){
                exception.printStackTrace();
            }
        }
        FileWriter fw = null;
        try{fw = new FileWriter(file.getAbsoluteFile());}
        catch (IOException exception){exception.printStackTrace();}
        BufferedWriter bufferedWriter = new BufferedWriter(fw);
        try{bufferedWriter.write(line);}
        catch (IOException exception){exception.printStackTrace();}
        finally {
            try{bufferedWriter.close();}
            catch (IOException exception){exception.printStackTrace();}
        }
    }
}

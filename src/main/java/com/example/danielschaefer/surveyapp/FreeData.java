package com.example.danielschaefer.surveyapp;

/**
 * Created by danielschaefer on 5/5/16.
 */
public class FreeData extends QuestionData{

    public FreeData(int questionNumber, int defaultNext, String prompt, boolean mandatory){
        super(questionNumber, defaultNext, prompt,mandatory);
    }

}
